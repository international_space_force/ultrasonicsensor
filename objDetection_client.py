from pymavlink import mavutil
from datetime import datetime
import json

connection = mavutil.mavlink_connection('udpin:192.168.1.236:4200')


def incoming_msg():
    while True:
        try:
            data = connection.recv_match().to_dict()
            now = datetime.now()
            date_time = now.strftime('%m-%d-%Y %H:%M:%S')
            with open('receiver.log', 'a') as file:
                file.write(date_time + " " + json.dumps(data) + "\n")

            print(data)
        except:
            pass


print("Listening for messages...")


try:
    while True:
        incoming_msg()
except KeyboardInterrupt:
    print("Stopped by user")
